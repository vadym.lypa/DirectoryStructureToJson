﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Common
{
    [DataContract]
    public class DirectoryNode
    {
        public DirectoryNode(string DirectoryName)
        {
            this.DirectoryName = DirectoryName;
            Children = new List<DirectoryNode>();
            Files = new List<FileNode>();
        }

        [DataMember(Name = "Name", IsRequired = true)]
        public string DirectoryName { get; set; }

        [DataMember(Name = "Files", IsRequired = false)]
        public List<FileNode> Files;

        [DataMember(Name = "Directory", IsRequired = false)]
        public List<DirectoryNode> Children;
    }
}