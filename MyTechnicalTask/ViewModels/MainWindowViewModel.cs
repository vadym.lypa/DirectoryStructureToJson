﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Windows.Forms;
using System.Windows.Input;
using Common;
using Extensibility;
using Prism.Commands;
using Prism.Mvvm;

namespace MyTechnicalTask.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        #region Fields

        private string _folderPath;
        private string _outputConvertedJson;
        private bool _isConvertedButtonEnabled;

        private DirectoryNode _directories;
        private readonly INotificationService _notificationService;

        #endregion

        #region Properties

        public MainWindowViewModel(INotificationService notificationService)
        {
            _notificationService = notificationService;
        }

        public bool IsConvertedButtonEnabled
        {
            get { return _isConvertedButtonEnabled; }
            set { SetProperty(ref _isConvertedButtonEnabled, value); }
        }

        public string OutputConvertedJson
        {
            get { return _outputConvertedJson; }
            set { SetProperty(ref _outputConvertedJson, value); }
        }

        public string FolderPath
        {
            get { return _folderPath; }
            set { SetProperty(ref _folderPath, value); }
        }

        #endregion

        #region Commands

        public ICommand SelectFolderPathCommand => new DelegateCommand(OnSelectedFolder);
        public ICommand ConvertToJsonCommand => new DelegateCommand(OnConvertedToJson);

        #endregion

        #region Methods

        private void OnSelectedFolder()
        {
            var browserDialog = new FolderBrowserDialog();
            browserDialog.ShowDialog();

            if (!string.IsNullOrWhiteSpace(browserDialog.SelectedPath))
            {
                FolderPath = browserDialog.SelectedPath;
                IsConvertedButtonEnabled = true;
            }
        }

        private void OnConvertedToJson()
        {
            var directoryInfo = new DirectoryInfo(FolderPath);
            _directories = new DirectoryNode(directoryInfo.Name);

            WalkDirectoryTree(_directories, directoryInfo);
            File.WriteAllText(Path.Combine(FolderPath, "directory.json"), SerializeToJson(_directories));
        }

        private void WalkDirectoryTree(DirectoryNode directoryNode, DirectoryInfo directoryInfo)
        {
            var subDirectory = new List<DirectoryInfo>();
            FileInfo[] files;
            try
            {
                files = directoryInfo.GetFiles("*.*");
            }
            catch (UnauthorizedAccessException e)
            {
                _notificationService.ErrorConvert(e.Message);
                return;
            }
            catch (DirectoryNotFoundException e)
            {
                _notificationService.ErrorConvert(e.Message);
                return;
            }

            foreach (FileInfo file in files)
            {
                directoryNode.Files.Add(new FileNode
                {
                    Name = file.Name,
                    FileSize = file.Length.ToString(),
                    Path = file.DirectoryName
                });
            }

            subDirectory.AddRange(directoryInfo.GetDirectories());
            foreach (DirectoryInfo dirInfo in subDirectory)
            {
                var dirNode = new DirectoryNode(dirInfo.Name);
                directoryNode.Children.Add(dirNode);

                WalkDirectoryTree(dirNode, dirInfo);
            }
        }

        private string SerializeToJson(DirectoryNode WorkDirectory)
        {
            return Serialize(WorkDirectory);
        }

        private string Serialize<T>(T Obj)
        {
            string getJson;
            try
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(Obj.GetType());
                using (MemoryStream ms = new MemoryStream())
                {
                    serializer.WriteObject(ms, Obj);
                    getJson = Encoding.Default.GetString(ms.ToArray());
                    OutputConvertedJson = FormatOutput(getJson);

                    _notificationService.SuccessfulConvertToJson();
                }
            }
            catch (Exception ex)
            {
                _notificationService.ErrorConvert(ex.Message);
                getJson = "";
            }

            return getJson;
        }

        #region FormatOutput Json

        private string FormatOutput(string jsonFormatted)
        {
            var stringBuilder = new StringBuilder();

            bool escaping = false;
            bool inQuotes = false;
            int indentation = 0;

            foreach (char character in jsonFormatted)
            {
                if (escaping)
                {
                    escaping = false;
                    stringBuilder.Append(character);
                }
                else
                {
                    switch (character)
                    {
                        case '\\':
                            escaping = true;
                            stringBuilder.Append(character);
                            break;
                        case '\"':
                            inQuotes = !inQuotes;
                            stringBuilder.Append(character);
                            break;
                        default:
                            if (!inQuotes)
                            {
                                switch (character)
                                {
                                    case ',':
                                        stringBuilder.Append(character);
                                        stringBuilder.Append("\r\n");
                                        stringBuilder.Append('\t', indentation);
                                        break;
                                    case '{':
                                    case '[':
                                        stringBuilder.Append(character);
                                        stringBuilder.Append("\r\n");
                                        stringBuilder.Append('\t', ++indentation);
                                        break;
                                    case '}':
                                    case ']':
                                        stringBuilder.Append("\r\n");
                                        stringBuilder.Append('\t', --indentation);
                                        stringBuilder.Append(character);
                                        break;
                                    case ':':
                                        stringBuilder.Append(character);
                                        stringBuilder.Append('\t');
                                        break;
                                    default:
                                        stringBuilder.Append(character);
                                        break;
                                }
                            }
                            else
                            {
                                stringBuilder.Append(character);
                            }
                            break;
                    }
                }
            }

            return stringBuilder.ToString();
        }

        #endregion

        #endregion

    }
}

