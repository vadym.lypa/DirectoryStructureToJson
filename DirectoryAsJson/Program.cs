﻿using System;
using System.IO;
using Newtonsoft.Json.Linq;

namespace DirectoryAsJson
{
    class Program
    {
        static void Main(string[] args)
        {
            string json = GetDirectoryAsJson(@"E:\googledrive");
            Console.WriteLine($"Output : {json}");
            Console.ReadKey();
        }

        public static string GetDirectoryAsJson(string path)
        {
            return GetDirectoryAsJObject(new DirectoryInfo(path)).ToString();
        }

        public static JObject GetDirectoryAsJObject(DirectoryInfo directory)
        {
            var obj = new JObject();
            foreach (DirectoryInfo dir in directory.EnumerateDirectories())
            {
                obj.Add(dir.Name, GetDirectoryAsJObject(dir));
            }
            foreach (FileInfo file in directory.GetFiles())
            {
                obj.Add(file.Name, JValue.CreateString("--file"));
            }
            return obj;
        }
    }
}